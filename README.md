# AA Team Assignment 2021

This is the repository for Team 02 in the course Analytics and Applications by Prof. Dr. Wolfgang Ketter.

## Name
Team Assignment 2021 Group 2

## Description
Team Assignment for course Analytics and Applications by Prof. Dr. Wolfgang Ketter

## Authors and acknowledgment
Lazar Milosevic
Foad Biglaripour
Nathalie Spellerberg
Vanessa Kremer
Lars Schiffer